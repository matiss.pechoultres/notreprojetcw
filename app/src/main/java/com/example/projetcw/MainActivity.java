package com.example.projetcw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void changerHabitudes(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), ChoixHabitudes.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerCalendar(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), Calendar.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerCarbonne(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), CalculCarbone.class);
        startActivity(messageVersActivity);
        finish();
    }
    public void changerBravo(View view) {
        // boutton temporaire pour changer les page et les tester
        Intent messageVersActivity = new Intent();
        messageVersActivity.setClass(getApplicationContext(), BravoActivity.class);
        startActivity(messageVersActivity);
        finish();
    }
}